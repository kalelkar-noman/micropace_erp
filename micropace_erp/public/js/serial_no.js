frappe.ui.form.on('Serial No', {
  refresh: function (frm) {
    frm.add_custom_button(__('Update Posting Date'), function () {
      fetch_latest_onestim_update(frm);
    });
  },
});

// Fetch the latest OneStim Update record for the Serial No
function fetch_latest_onestim_update(frm) {
  frappe.call({
    method: 'frappe.client.get_list',
    type: 'GET',
    args: {
      doctype: 'OneStim Update',
      fields: ['name', 'posting_date'],
      filters: { serial_no: frm.doc.serial_no },
      order_by: 'creation desc',
      limit: 1,
    },
    callback: function (response) {
      let latest_onestim_update = '';
      let latest_posting_date = '';

      if (response.message && response.message.length > 0) {
        const { name, posting_date } = response.message[0];
        latest_onestim_update = name;
        latest_posting_date = posting_date
          ? posting_date
          : frappe.datetime.get_today();
      }

      prompt_for_update(frm, latest_onestim_update, latest_posting_date);
    },
  });
}

// Prompt user to update the Posting Date for the OneStim Update record
function prompt_for_update(frm, latest_onestim_update, latest_posting_date) {
  frappe.prompt(
    [
      {
        fieldname: 'onestim_update',
        fieldtype: 'Link',
        default: latest_onestim_update,
        options: 'OneStim Update',
        label: 'OneStim Update',
        reqd: true,
        get_query: function () {
          return {
            filters: {
              serial_no: frm.doc.serial_no,
            },
          };
        },
      },
      {
        fieldname: 'posting_date',
        fieldtype: 'Date',
        label: 'Posting Date',
        default: latest_posting_date,
        reqd: true,
      },
    ],
    function (values) {
      update_posting_date(values);
    },
    'Update date',
    'Update',
  );
}

// Update the Posting Date for the selected OneStim Update record
function update_posting_date(values) {
  frappe.call({
    method: 'frappe.client.set_value',
    args: {
      doctype: 'OneStim Update',
      name: values.onestim_update,
      fieldname: 'posting_date',
      value: values.posting_date,
    },
    callback: function (response) {
      if (!response.exc) {
        frappe.show_alert(
          {
            message: __('Posting Date updated successfully.'),
            indicator: 'green',
          },
          5,
        );
      } else {
        frappe.show_alert(
          {
            message: __('An error occurred'),
            indicator: 'red',
          },
          5,
        );
      }
    },
  });
}
