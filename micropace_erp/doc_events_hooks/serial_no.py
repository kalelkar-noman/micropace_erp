import frappe
import frappe.desk
import frappe.desk.form
import frappe.desk.form.linked_with
from frappe.desk.form.linked_with import get_linked_docs
from frappe.model.document import Document

from micropace_erp.constants import TRANSACTIONAL_DOCTYPES_WITH_CHILD_TABLE

from micropace_erp.version_history.create_version_history import (  # noqa: 501 isort: skip
    create_version_history,
)


def on_update(self, method=None):
    update_version_history(self)


def update_version_history(serial_no_doc: Document):
    """
    Creates Version History
    if software or hardvare version is changed
    """

    if serial_no_doc.flags.in_insert:
        return

    location = get_customer_or_warehouse(serial_no_doc)

    has_version_changed = serial_no_doc.has_value_changed(
        "custom_software_version"
    ) or serial_no_doc.has_value_changed("custom_hardware_version")

    if has_version_changed:
        create_version_history(
            serial_no=serial_no_doc.name,
            software_version=serial_no_doc.custom_software_version,
            hardware_version=serial_no_doc.custom_hardware_version,
            customer=location.customer,
            warehouse=location.warehouse,
            posting_date=frappe.utils.today(),
        )


def get_customer_or_warehouse(serial_no_doc):
    # get all the linked Batch Bundle of current Serial No
    link_info = {
        "Serial and Batch Bundle": {
            "child_doctype": "Serial and Batch Entry",
            "fieldname": ["serial_no"],
            "doctype": "Serial and Batch Bundle",
            "add_fields": ["voucher_no"],
        }
    }

    serial_no_links = get_linked_docs(
        "Serial No", serial_no_doc.name, link_info
    )  # noqa:501

    # Filter out non-submitted bundles or bundles with no voucher number
    serial_and_batch_bundles = []

    for links in serial_no_links.get("Serial and Batch Bundle"):
        if links.docstatus == 0 or (not links.voucher_no):
            continue
        serial_and_batch_bundles.append(links)

    # Get the latest Serial and Batch Bundle
    # based on posting date and posting time
    if serial_and_batch_bundles:

        latest_serial_and_batch_bundle = frappe.get_all(
            "Serial and Batch Bundle",
            filters=[
                ["name", "in", [s.name for s in serial_and_batch_bundles]]
            ],  # noqa: 501
            order_by="posting_date DESC,  posting_time DESC",
            limit=1,
        )

        if len(latest_serial_and_batch_bundle):

            voucher_type, voucher_no = next(
                (
                    (bundle.voucher_type, bundle.voucher_no)
                    for bundle in serial_and_batch_bundles
                    if latest_serial_and_batch_bundle[0].name
                    == bundle.name  # noqa: 501
                ),
                (None, None),
            )

            def get_location(location):
                return frappe.db.get_value(
                    TRANSACTIONAL_DOCTYPES_WITH_CHILD_TABLE.get(voucher_type),
                    {"parenttype": voucher_type, "parent": voucher_no},
                    location,
                )

            warehouse = None
            customer = None

            # consider Costomer from Delivery Note if its not returned,
            # else consider warehouse for every other Voucher
            if voucher_type == "Delivery Note":
                is_return, customer = frappe.db.get_value(
                    voucher_type, voucher_no, ["is_return", "customer"]
                )

                if is_return:
                    customer = None
                    warehouse = get_location("warehouse")

            elif voucher_type == "Purchase Receipt":
                warehouse = get_location("warehouse")

            elif voucher_type == "Stock Entry":
                warehouse = get_location("t_warehouse")

    return frappe._dict({"warehouse": warehouse, "customer": customer})
