import frappe


def before_submit(self, method=None):
    for entry in self.entries:
        entry.custom_submitted_on = frappe.utils.now()
