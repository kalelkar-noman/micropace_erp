import frappe
from frappe.modules.utils import sync_customizations


def execute():
    """
    Serial and Batch Entry:
    Copying `modified` date into `custom_submitted_on`
    """
    sync_customizations()

    frappe.db.sql(
        "UPDATE `tabSerial and Batch Entry` SET custom_submitted_on=modified"
    )  # noqa: 501
    frappe.db.commit()
