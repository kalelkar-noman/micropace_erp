import frappe


def execute():
    """
    Removing Micropace Version History report
    """
    remove_report()


def remove_report():
    if frappe.db.exists("Report", "Micropace Version History"):
        report = frappe.get_doc("Report", "Micropace Version History")
        report.delete()
