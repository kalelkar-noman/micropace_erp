import frappe


def execute():
    populate_sw_hw_in_serial_batch_entry()


def populate_sw_hw_in_serial_batch_entry():
    one_stim_updates = frappe.get_all(
        "OneStim Update",
        filters=[["self_update", "=", 0]],
        fields=[
            "voucher_item_type",
            "voucher_item_no",
            "hardware_version",
            "software_version",
        ],
    )

    for ou in one_stim_updates:
        serial_and_batch_bundle = frappe.get_value(
            ou.voucher_item_type, ou.voucher_item_no, "serial_and_batch_bundle"
        )

        serial_and_batch_entries = frappe.get_all(
            "Serial and Batch Entry",
            filters=[["parent", "=", serial_and_batch_bundle]],  # noqa: 501
        )

        for entry in serial_and_batch_entries:
            frappe.db.set_value(
                "Serial and Batch Entry",
                entry.name,
                {
                    "custom_software_version": ou.software_version,
                    "custom_hardware_version": ou.hardware_version,
                },
            )

        frappe.db.delete("OneStim Update", ou)
