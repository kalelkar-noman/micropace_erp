import frappe


def execute():
    """
    Copy Micropace Version History data to OneStim Update
    Delete Microppace Version History Doctype
    """
    copy_micropace_version_history_to_onestim_update()
    delete_micropace_version_history_doctype()


def copy_micropace_version_history_to_onestim_update():

    query = """SELECT * FROM `tabMicropace Version History`"""

    version_history_data = frappe.db.sql(query, as_dict=True)

    for data in version_history_data:
        onestim_doc = frappe.get_doc({**data, "doctype": "OneStim Update"})

        onestim_doc.insert()


def delete_micropace_version_history_doctype():
    if frappe.db.exists("DocType", "Micropace Version History"):
        dt = frappe.get_doc("DocType", "Micropace Version History")
        dt.delete()
