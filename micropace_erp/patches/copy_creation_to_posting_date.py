import frappe


def execute():
    copy_creation_to_posting_date()


def copy_creation_to_posting_date():
    one_stim_updates = frappe.get_all(
        "OneStim Update",
        fields=["posting_date", "creation", "name"],
    )

    for doc in one_stim_updates:
        posting_date = doc.creation.date()
        frappe.db.set_value(
            "OneStim Update", doc.name, "posting_date", posting_date  # noqa:501
        )

    frappe.db.commit()
