# Copyright (c) 2024, Micropace Pty Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe import _


def execute(filters=None):
    columns = get_columns()
    data = get_data(filters)
    return columns, data


def get_columns():
    return [
        {
            "label": _("Date"),
            "fieldname": "date",
            "fieldtype": "Date",
            "width": 120,
        },
        {
            "label": _("Item"),
            "fieldname": "item_code",
            "fieldtype": "Data",
            "width": 100,
        },
        {
            "label": _("Serial No"),
            "fieldname": "serial_no",
            "fieldtype": "Data",
            "width": 90,
        },
        {
            "label": _("Voucher Type"),
            "fieldname": "voucher_type",
            "fieldtype": "Data",
            "width": 150,
        },
        {
            "label": _("Voucher Number"),
            "fieldname": "voucher_no",
            "fieldtype": "Data",
            "width": 200,
        },
        {
            "label": _("Customer/Warehouse"),
            "fieldname": "customer_or_warehouse",
            "fieldtype": "Data",
            "width": 170,
        },
        {
            "label": _("Software Version"),
            "fieldname": "custom_software_version",
            "fieldtype": "Data",
            "width": 120,
        },
        {
            "label": _("PCBA Set"),
            "fieldname": "custom_hardware_version",
            "fieldtype": "Data",
            "width": 120,
        },
        {
            "label": _("Status"),
            "fieldname": "status",
            "fieldtype": "Data",
            "width": 100,
        },
        {
            "label": _("Purpose"),
            "fieldname": "purpose",
            "fieldtype": "Data",
            "width": 130,
        },
        {
            "label": _("Is Return"),
            "fieldname": "is_return",
            "fieldtype": "Check",
            "width": 100,
        },
    ]


def get_data(filters=None):
    query = f"""
                SELECT
                    sbe.serial_no,
                    sbe.custom_software_version,
                    sbe.custom_hardware_version,
                    sbb.item_code,
                    sbb.voucher_no,
                    sbb.voucher_type,
                    CASE
                        WHEN sbb.voucher_type = "Delivery Note" THEN
                            CASE
                                WHEN dn.is_return is True THEN sbe.warehouse
                                ELSE dn.customer
                            END
                        ELSE
                            sbb.warehouse
                    END AS customer_or_warehouse,
                    sbb.posting_date AS date,
                    sbb.posting_time AS time,
                    dn.custom_purpose as purpose,
                    dn.is_return,
                    CASE
                        WHEN sbb.voucher_type = "Delivery Note" THEN dn.status
                        WHEN sbb.voucher_type = "Purchase Receipt" THEN pr.status
                        ELSE ste.docstatus
                    END AS status
                FROM
                    `tabSerial and Batch Entry` sbe
                LEFT JOIN
                    `tabSerial and Batch Bundle` sbb ON sbb.name = sbe.parent
                LEFT JOIN
                    `tabDelivery Note` dn ON dn.name = sbb.voucher_no
                    AND
                        sbb.voucher_type = 'Delivery Note'
                LEFT JOIN
                    `tabPurchase Receipt` pr ON pr.name = sbb.voucher_no
                    AND
                        sbb.voucher_type = 'Purchase Receipt'
                LEFT JOIN
                    `tabStock Entry` ste ON ste.name = sbb.voucher_no
                    AND
                        sbb.voucher_type = 'Purchase Receipt'
                {get_filter_conditions(filters)}

                UNION

                SELECT
                    oe.serial_no,
                    oe.software_version AS custom_software_version,
                    oe.hardware_version AS custom_hardware_version,
                    sno.item_code,
                    oe.name AS voucher_no,
                    'Self Update' AS voucher_type,
                    CASE
                        WHEN oe.customer IS NOT NULL THEN oe.customer
                        ELSE oe.warehouse
                    END AS customer_or_warehouse,
                    oe.posting_date AS date,
                    NULL AS time,
                    NULL AS purpose,
                    NULL AS is_return,
                    NULL AS status
                FROM
                    `tabOneStim Update` oe
                LEFT JOIN
                    `tabSerial No` sno ON sno.name = oe.serial_no
                {get_self_update_filter_conditions(filters)}

                ORDER BY
                    date DESC, time DESC;
            """  # noqa: 501
    data = frappe.db.sql(
        query,
        as_dict=1,
    )

    if filters.get("last_record"):
        latest_records = {}

        for record in data:
            serial_no = record["serial_no"]
            if serial_no not in latest_records:
                latest_records[serial_no] = record
            else:
                # Compare dates to update with the latest record
                if record["date"] > latest_records[serial_no]["date"]:
                    latest_records[serial_no] = record

        latest_records = list(latest_records.values())
        data = latest_records

    for entry in data:
        if entry.voucher_type != "Delivery Note":
            if entry.voucher_type == "Stock Entry":
                entry.status = (
                    "Submitted"
                    if frappe.db.get_value(
                        entry.voucher_type, entry.voucher_no, "docstatus"
                    )
                    else "Draft"
                )
            else:
                entry.status = frappe.db.get_value(
                    entry.voucher_type, entry.voucher_no, "status"
                )
    return data


def get_self_update_filter_conditions(filters=None):
    conditions = []

    if filters:
        if filters.get("from_date") and filters.get("to_date"):
            conditions.append(
                f"DATE(oe.creation) BETWEEN DATE('{filters.get('from_date')}') AND DATE('{filters.get('to_date')}')"  # noqa: 501
            )

        if filters.get("serial_no"):
            conditions.append(f"oe.serial_no = '{filters.get('serial_no')}'")

        if filters.get("warehouse"):
            conditions.append(f"oe.warehouse = '{filters.get('warehouse')}'")

        if filters.get("sw_version"):
            conditions.append(
                f"oe.software_version = '{filters.get('sw_version')}'"
            )  # noqa: 501

        if filters.get("hw_version"):
            conditions.append(
                f"oe.hardware_version = '{filters.get('hw_version')}'"
            )  # noqa: 501

        if filters.get("item_code"):
            conditions.append(f"sno.item_code = '{filters.get('item_code')}'")

    return "WHERE " + " AND ".join(conditions) if conditions else ""


def get_filter_conditions(filters=None):
    conditions = []

    if filters:
        if filters.get("from_date") and filters.get("to_date"):
            conditions.append(
                f"DATE(sbe.custom_submitted_on) BETWEEN DATE('{filters.get('from_date')}') AND DATE('{filters.get('to_date')}')"  # noqa: 501
            )

        if filters.get("serial_no"):
            conditions.append(f"sbe.serial_no = '{filters.get('serial_no')}'")

        if filters.get("warehouse"):
            conditions.append(f"sbe.warehouse = '{filters.get('warehouse')}'")

        if filters.get("sw_version"):
            conditions.append(
                f"sbe.custom_software_version = '{filters.get('sw_version')}'"
            )

        if filters.get("hw_version"):
            conditions.append(
                f"sbe.custom_hardware_version = '{filters.get('hw_version')}'"
            )

        if filters.get("item_code"):
            conditions.append(f"sbb.item_code = '{filters.get('item_code')}'")

    conditions.append("sbe.docstatus = 1")

    return "WHERE " + " AND ".join(conditions) if conditions else ""
