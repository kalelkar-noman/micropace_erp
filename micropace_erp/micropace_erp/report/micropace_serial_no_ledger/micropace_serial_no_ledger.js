// Copyright (c) 2024, Micropace Pty Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports['Micropace Serial No Ledger'] = {
  filters: [
    {
      label: 'Serial Number',
      fieldname: 'serial_no',
      fieldtype: 'Link',
      options: 'Serial No',
    },
    {
      label: 'Item',
      fieldname: 'item_code',
      fieldtype: 'Link',
      options: 'Item',
    },
    {
      label: 'From Date',
      fieldname: 'from_date',
      fieldtype: 'Date',
      default: frappe.datetime.add_months(frappe.datetime.get_today(), -1),
    },
    {
      label: 'To Date',
      fieldname: 'to_date',
      fieldtype: 'Date',
      default: frappe.datetime.get_today(),
    },
    {
      label: 'Show Latest Records Only',
      fieldname: 'last_record',
      fieldtype: 'Check',
      default: 1,
    },
  ],
};
