// Copyright (c) 2024, Micropace Pty Ltd. and contributors
// For license information, please see license.txt
/* global frappe */

frappe.query_reports['Micropace Stock Report'] = {
  filters: [
    {
      label: 'Serial Number',
      fieldname: 'serial_no',
      fieldtype: 'Link',
      options: 'Serial No',
    },
    // {
    //   label: 'Warehouse',
    //   fieldname: 'warehouse',
    //   fieldtype: 'Link',
    //   options: 'Warehouse',
    // },
    // {
    //   label: 'Software Version',
    //   fieldname: 'sw_version',
    //   fieldtype: 'Data',
    // },
    // {
    //   label: 'PCBA Set',
    //   fieldname: 'hw_version',
    //   fieldtype: 'Data',
    // },
    {
      label: 'Item',
      fieldname: 'item_code',
      fieldtype: 'Link',
      options: 'Item',
      reqd: 1,
    },
    {
      label: 'From Date',
      fieldname: 'from_date',
      fieldtype: 'Date',
    },
    {
      label: 'To Date',
      fieldname: 'to_date',
      fieldtype: 'Date',
    },
  ],
};
