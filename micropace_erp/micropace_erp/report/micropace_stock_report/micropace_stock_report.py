# Copyright (c) 2024, Micropace Pty Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe import _


def execute(filters=None):
    columns = get_columns()
    data = get_data(filters)
    return columns, data


def get_returned_value(columns, data):
    return columns, data


def get_columns():
    return [
        {
            "label": _("Date"),
            "fieldname": "creation",
            "fieldtype": "Date",
            "width": 120,
        },
        {
            "label": _("Serial No."),
            "fieldname": "serial_no",
            "fieldtype": "Link",
            "options": "Serial No",
            "width": 120,
        },
        {
            "label": _("Voucher Type"),
            "fieldtype": "Data",
            "fieldname": "voucher_type",
            "width": 150,
        },
        {
            "label": _("Voucher Number"),
            "fieldtype": "Dynamic Link",
            "fieldname": "voucher_no",
            "options": "voucher_type",
            "width": 200,
        },
        {
            "label": _("Warehouse"),
            "fieldtype": "Link",
            "fieldname": "warehouse",
            "options": "Warehouse",
            "width": 180,
        },
        {
            "label": _("Software Version"),
            "fieldname": "software_version",
            "fieldtype": "Data",
            "width": 150,
        },
        {
            "label": _("PCBA Set"),
            "fieldname": "hardware_version",
            "fieldtype": "Data",
            "width": 100,
        },
        {
            "label": _("Customer"),
            "fieldname": "customer",
            "fieldtype": "Link",
            "options": "Customer",
            "width": 150,
        },
        {
            "label": _("Status"),
            "fieldname": "status",
            "fieldtype": "Data",
            "width": 100,
        },
        {
            "label": _("Purpose"),
            "fieldname": "purpose",
            "fieldtype": "Data",
            "width": 150,
        },
        {
            "label": _("Is Return"),
            "fieldname": "is_return",
            "fieldtype": "Check",
            "width": 100,
        },
    ]


def get_data(filters=None):

    query = f""" SELECT * FROM `tabOneStim Update` osu {get_filter_conditions(filters)} ORDER BY osu.creation DESC """  # noqa: 501

    data = frappe.db.sql(
        query,
        as_dict=1,
    )

    for entry in data:
        if entry.voucher_item_no:

            if entry.voucher_type == "Delivery Note":
                voucher = get_voucher(
                    child_table=entry.voucher_item_type,
                    parent_table=entry.voucher_type,
                    voucher_no=entry.voucher_no,
                    child_fields=["item_code", "warehouse"],
                    parent_fields=[
                        "status",
                        "custom_purpose",
                        "customer",
                        "is_return",
                    ],  # noqa: 501
                )

                entry.status = voucher.get("status")
                entry.customer = voucher.get("customer")
                entry.purpose = voucher.get("custom_purpose")
                entry.is_return = int(voucher.get("is_return"))

            if entry.voucher_type == "Purchase Receipt":
                voucher = get_voucher(
                    child_table=entry.voucher_item_type,
                    parent_table=entry.voucher_type,
                    voucher_no=entry.voucher_no,
                    child_fields=["item_code", "warehouse"],
                    parent_fields=["status"],
                )
                entry.warehouse = voucher.get("warehouse")
                entry.status = voucher.get("status")

            if entry.voucher_type == "Stock Entry":
                voucher = get_voucher(
                    child_table=entry.voucher_item_type,
                    parent_table=entry.voucher_type,
                    voucher_no=entry.voucher_no,
                    child_fields=["item_code", "t_warehouse"],
                    parent_fields=["docstatus"],
                )

                entry.status = (
                    "Submitted" if voucher.get("docstatus") else "Draft"
                )  # noqa: 501
                entry.warehouse = voucher.get("t_warehouse")

        else:
            # for self update
            entry.voucher_type = "OneStim Update"
            entry.voucher_no = entry.name

    return data


def get_voucher(
    child_table, parent_table, voucher_no, child_fields=[], parent_fields=[]
):

    child_fields = [f"ct.{field}" for field in child_fields]
    parent_fields = [f"pt.{field}" for field in parent_fields]
    fields = child_fields + parent_fields
    fields = ", ".join(fields)

    query = """
        SELECT {fields}
        FROM `tab{child_table}` ct
        LEFT JOIN `tab{parent_table}` pt ON ct.parent = pt.name
        WHERE pt.name = '{voucher_no}'
    """.format(
        fields=fields,
        child_table=child_table,
        parent_table=parent_table,
        voucher_no=voucher_no,
    )

    return frappe.db.sql(query, as_dict=True)[0]


def get_filter_conditions(filters=None):
    condition = ""
    join = ""

    if not filters:
        return condition

    if filters.get("from_date") and filters.get("to_date"):
        condition += f""",DATE(osu.creation) BETWEEN DATE('{filters.get('from_date')}') AND DATE('{filters.get('to_date')}')"""  # noqa: 501

    if filters.get("serial_no"):
        condition += f""",osu.serial_no = '{filters.get("serial_no")}'"""

    if filters.get("warehouse"):
        condition += f""",osu.warehouse = '{filters.get("warehouse")}'"""

    if filters.get("sw_version"):
        condition += (
            f""",osu.software_version = '{filters.get("sw_version")}'"""  # noqa: 501
        )

    if filters.get("hw_version"):
        condition += (
            f""",osu.hardware_version = '{filters.get("hw_version")}'"""  # noqa: 501
        )

    if filters.get("item_code"):
        join = "LEFT JOIN `tabSerial No` sno ON sno.name = osu.serial_no "
        condition += f""",sno.item_code = '{filters.get("item_code")}'"""

    if condition:
        condition = join + "WHERE " + condition
        condition = condition.replace(",", "", 1)
        condition = condition.replace(",", " AND ")

    return condition
