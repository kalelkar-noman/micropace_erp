// Copyright (c) 2024, Micropace Pty Ltd. and contributors
// For license information, please see license.txt
/* global frappe */
frappe.ui.form.on('OneStim Update', {
  refresh(frm) {
    frm.disable_form();
  },
});
