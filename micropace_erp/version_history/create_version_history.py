import frappe


def create_version_history(
    serial_no,
    software_version="",
    hardware_version="",
    customer=None,
    warehouse=None,
    posting_date=frappe.utils.today(),  # noqa: 501
):
    doc = frappe.new_doc(
        "OneStim Update",
        serial_no=serial_no,
        software_version=software_version,
        hardware_version=hardware_version,
        customer=customer,
        warehouse=warehouse,
        posting_date=posting_date,
    ).insert(ignore_permissions=True)

    return doc
