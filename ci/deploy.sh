# backend
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/52e0e342-1e09-40c9-b407-5696fdd969f4 -o /dev/null

# frontend
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/bf65b76d-0c29-4a11-8560-54cf645bb963 -o /dev/null

# migration
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/d34965c9-a47e-4f81-ba00-dfd5641a8e3f -o /dev/null

# queue-default
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/17bbdfbd-e581-4234-b0bc-a0faefa20003 -o /dev/null

# queue-long
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/70992bfa-aae9-46dc-b7ae-6bde62ce82f0 -o /dev/null

# queue-short
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/ad84ebae-9885-44d6-af24-55632a061d29 -o /dev/null

# scheduler
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/cd736f61-5cf4-45df-a23b-aa9b284f7987 -o /dev/null

# websocket
curl --insecure -s -X POST https://portainer.micropace.com.au/api/webhooks/3bf77295-53ec-44dd-87c4-99d319ab90c8 -o /dev/null
